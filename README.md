# Font retrieval/install

## Scope

This setup takes care of retrieval and installation of fonts,
the setup for desktop and apps is done elsewhere / manually.

## Contents

* fonts.cfg
List NAME URL to download zipped fonts.
Fonts are extracted in a cache directory.

## Compatibility
Compatible with systems that scour the "${HOME}/.fonts" directory.

