#!/bin/bash
# font setup
#
# martinx (28/07/2023)

set -o nounset
set -e

### begin parameters
VERBOSE=${VERBOSE:-""}
FONT_FILE=${FONT_FILE:-"fonts.cfg"}
DIR_DOWNLOAD=${DIR_DOWNLOAD:-"${HOME}/.cache/fontDownload"}
TARGET_DIR=${TARGET_DIR:-"${HOME}/.fonts"}
### end parameters

## Verbosity flags: 
[[ -z ${VERBOSE} ]] && WGET_VERBOSITY="--no-verbose --show-progress" || WGET_VERBOSITY=""
[[ -z ${VERBOSE} ]] && FCCACHE_VERBOSITY="" || FCCACHE_VERBOSITY="-v"


function status() { echo "=== font setup ===" "$@"; }
function statusVerbose() { [[ ! -z $VERBOSE ]] && status "$@"; return 0; }

function tryDownloadFont() {
	# PARAM: name url
	# RET: OK <- 0; FAIL <- 1
	local name="$1"
	local url="$2"
	[[ $name != "" ]] || { status "FAIL tryDownloadFont: empty 'name'"; exit 1; }
	[[ $url != "" ]] || { status "FAIL tryDownloadFont: empty 'url'"; exit 1; }

	status "Downloading... $name $url"
	wget -P "$DIR_DOWNLOAD" \
	     --no-clobber \
	     ${WGET_VERBOSITY} \
	     "${url}"
	if [[ $? != 0 ]]; then
		status "WARNING: could not fetch $name at $url"
		return 1
	fi
}

function isFontInstalled() {
	# PARAM: name, glob(TARGET_DIR)
	(( $# == 1 )) || { status "FAIL isFontInstalled: wrong use"; exit 1; }
	local name="$1"
	if [[ -d "${TARGET_DIR}/${name}" ]]; then
		return 0
	else
		return 1
	fi
}

function tryDownloadAllFonts() {
# PARAM: VOID
	mkdir -p "$DIR_DOWNLOAD"
	while read -r name url; do
		[[ $name =~ ^[[:space:]]*# ]] && {
			statusVerbose "Skipping comment..";
			continue;
		}
		[[ $name =~ ^[[:space:]]*$ ]] && {
			statusVerbose "Skipping empty line..";
			continue;
		}
		if isFontInstalled $name; then
			status "INFO: skipping $name (found in $TARGET_DIR)."
		else
			tryDownloadFont "$name" "$url" || true # allow for failed fetch
		fi
	done < ${FONT_FILE}
}

function checkZipHealth() {
# PARAM: fileToCheck
	(( $# == 1 )) || {
		status "FAIL checkZipHealth: wrong use (expect 1 argument)"
		exit 1
	}
	local f=$1
	{ unzip -l $f; } || {
		status "ERROR: $f not a valid .zip file."
		status "NOTE: recommend running rm \"${f}\""
		return 1
	}
	return 0
}

function unpackAllFonts() {
# PARAM: VOID
	mkdir -p $TARGET_DIR
	[[ -w ${TARGET_DIR} ]] || {
		status "FAIL unpackAllFonts: no write permission to target dir";
		exit 1;
	}
	[[ -d "${DIR_DOWNLOAD}" ]] || {
		status "FAIL unpackAllFonts: no download dir found";
		exit 1;
	}
	while read -r f; do
		# only handles .zip files for simplicity
		[[ $f =~ .*\.zip$ ]] || {
			status "FAIL unpackAllFonts: file $f is not a .zip";
			exit 1;
		}
		local basename=$( basename "$f" )
		basename=${basename%.zip}
		if [[ -d ${TARGET_DIR}/${basename} ]]; then
			status "$basename already unpacked, skipping."
		else
			checkZipHealth "$f"
			status "Unpacking $basename..."
			unzip $f -d "${TARGET_DIR}/${basename}/"
		fi
	done < <( find "$DIR_DOWNLOAD" -type f )
}

function updateFontCache() {
# PARAM: VOID
	status "Updating font cache..."
	fc-cache -f $FCCACHE_VERBOSITY
}

tryDownloadAllFonts
unpackAllFonts
updateFontCache

